#pragma once

class TicTacToe 
{
private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

	const int m_winningCombos[8][3]{ {0,1,2}, {3,4,5}, {6,7,8}, {0,3,6}, {1,4,7}, {2,5,8}, {0,4,8}, {2,4,6} };

	bool CheckWinner() {
		for (int i = 0; i < 8; i++) {
			for (int u = 0; u < 3; u++) {
				if (m_board[m_winningCombos[i][u]] != m_playerTurn)
					break;
				if(u == 2)
					return true;
			}
		}
		return false;
	}

public:

	TicTacToe() {
		m_board[0] = '1';
		m_board[1] = '2';
		m_board[2] = '3';
		m_board[3] = '4';
		m_board[4] = '5';
		m_board[5] = '6';
		m_board[6] = '7';
		m_board[7] = '8';
		m_board[8] = '9';

		m_numTurns = 0;
		m_playerTurn = 'x';
		m_winner = ' ';
	}

	void DisplayBoard() {
		std::cout << m_board[0] << "|" << m_board[1] << "|" << m_board[2] << "\n";
		std::cout << "=====\n";
		std::cout << m_board[3] << "|" << m_board[4] << "|" << m_board[5] << "\n";
		std::cout << "=====\n";
		std::cout << m_board[6] << "|" << m_board[7] << "|" << m_board[8] << "\n\n";
	}

	bool IsOver() {
		if (m_numTurns == 9)
			return true;
		else
			return false;
	}

	char GetPlayerTurn() {
		return m_playerTurn;
	}

	bool IsValidMove(int position) {
		if ((position > 0 && position <= 9) && (m_board[position - 1] != 'x' && m_board[position - 1] != 'o'))
			return true;
		else
			return false;
	}

	void Move(int position) {
		m_board[position - 1] = m_playerTurn;
		if (CheckWinner()) {
			m_numTurns = 9;
			m_winner = m_playerTurn;
		}
		else 
		{
			m_numTurns++;
			m_playerTurn = m_playerTurn == 'x' ? 'o' : 'x';
		}
	}

	void DisplayResult() {
		if (m_winner != ' ') {
			std::cout << "The Winner is: " << m_winner << "!\n";
		}
		else {
			std::cout << "It's a tie.\n";
		}
	}
};